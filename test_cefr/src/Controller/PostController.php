<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Post;
use App\Entity\Commentaire;
use App\Form\FormPost;
use App\Form\FormCommentaire;
use App\Repository\CommentaireRepository;
//use Doctrine\Common\Collections\Collection;
//use Symfony\Component\Validator\Constraints\Collection;

class PostController extends AbstractController
{
    /**
     * @Route("/", name="posts")
     */
    public function index(Request $request)
    {
        $postRepository = $this->getDoctrine()->getManager()->getRepository('App\Entity\Post');
        $posts = $postRepository->findAll();

        return $this->render('post/index.html.twig', [
            'posts' => $posts,
        ]);
    }

    /**
     * @Route("/nouveauPost", name="nouveauPost")
     */
    public function nouveauPost(Request $request)
    {
        $post = new Post();
        $form = $this->createForm(FormPost::class, $post);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
            $post->setDateCreation(new \DateTime('NOW'));
            $post->setUtilisateur($this->get('security.token_storage')->getToken()->getUser());
            $post->setNombreLike(0);
            $entityManager = $this->getDoctrine()->getManager();            
            $entityManager->persist($post);
            $entityManager->flush();

            return $this->redirectToRoute('posts');
        }

        return $this->render('post/nouveauPost.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/post/{id}", name="post")
     */
    public function post($id, Request $request)
    {
        $postRepository = $this->getDoctrine()->getManager()->getRepository('App\Entity\Post');
        $post = $postRepository->find($id);
        //$commentaires = $post->getCommentaires();
        $commentaires=$post->getCommentaires();

        $commentaire = new Commentaire();
        $form = $this->createForm(FormCommentaire::class, $commentaire);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
            $commentaire->setDateCreation(new \DateTime('NOW'));
            $commentaire->setPost($post);
            $commentaire->setUtilisateur($this->get('security.token_storage')->getToken()->getUser());
            $entityManager = $this->getDoctrine()->getManager();            
            $entityManager->persist($commentaire);
            $entityManager->flush();

            return $this->redirect('/post/'.$id);
        }

        return $this->render('post/post.html.twig', [
            'post' => $post,
            'commentaires' => $commentaires,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/like/{id}", name="like")
     */
    public function like($id)
    {
        $postRepository = $this->getDoctrine()->getManager()->getRepository('App\Entity\Post');
        $post = $postRepository->find($id);
        $post->setNombreLike($post->getNombreLike()+1);
        $entityManager = $this->getDoctrine()->getManager();   
        $entityManager->remove($postRepository->find($id));         
        $entityManager->persist($post);
        $entityManager->flush();

        return $this->redirect('/post/'.$id);
    }

}
