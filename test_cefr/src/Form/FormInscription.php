<?php

namespace App\Form;

use App\Entity\Utilisateur;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;

class FormInscription extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom', TextType::class, array(
                'label' => 'Nom :',
                'attr' => array(
                    'class' => 'form-control',
                    'placeholder' => 'Quel est votre Nom ?'
                )
            ))

            ->add('prenom', TextType::class, array(
                'label' => 'Prénom :',
                'attr' => array(
                    'class' => 'form-control',
                    'placeholder' => 'Quel est votre Prenom ?'
                )
            ))
        
            ->add('identifiant', TextType::class, array(
                'label' => 'Identifiant :',
                'attr' => array(
                    'class' => 'form-control',
                    'placeholder' => 'Sous quel Pseudonyme voulez-vous apparaître dans le forum ?'
                )
            ))

            ->add('email', RepeatedType::class, array(
                'invalid_message' => 'Les deux Adresses Mails ne correspondent pas !',
                'type' => EmailType::class,
                'first_options' => array (
                    'label' => ' Adresse mail :',
                    'attr' => array(
                        'class' => 'form-control',
                        'placeholder' => 'Sur quelle adresse mail pouvons-nous vous contacter ?'
                    )
                ),
                'second_options' => array (
                    'label' => false,
                    'attr' => array(
                        'class' => 'form-control',
                        'placeholder' => 'Pour être bien sûr de votre mail, veuillez la réécrire'
                    )
                )
            ))

            ->add('motDePasse', RepeatedType::class, array(
                'invalid_message' => 'Les deux Mot De Passe ne correspondent pas !',
                'type' => PasswordType::class,
                'first_options' => array (
                    'label' => 'Mot de passe :',
                    'attr' => array(
                        'class' => 'form-control',
                        'placeholder' => 'Aidez-nous à sécuriser votre compte en renseignant un Mot de Passe'
                    )
                ),
                'second_options' => array (
                    'label' => false,
                    'attr' => array(
                        'class' => 'form-control',
                        'placeholder' => 'Pour être bien sûr de votre Mot De Passe, veuillez le réécrire'
                    )
                )
            ))

            ->add('submit', SubmitType::class, array(
                'label' => "S'inscrire",
                'attr' => array(
                    'class' => 'btn btn-success'
                )
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Utilisateur::class,
        ]);
    }
}
